name: Build Container Image

on:
  push:
    branches:
      - main
  pull_request:

jobs:
  container_image:
    runs-on: ubuntu-latest
    steps:
      - name: Checkout
        uses: actions/checkout@v3
        with:
          fetch-depth: 0

      - name: Set up QEMU
        uses: docker/setup-qemu-action@v2.0.0

      - name: Set up Docker Buildx
        uses: docker/setup-buildx-action@v2.0.0

      - name: Docker meta
        id: docker_meta
        uses: docker/metadata-action@v4.0.1
        with:
          images: ghcr.io/radiorabe/mediawiki
          flavor: |
            latest=auto

      - name: Login to GitHub Container Registry
        uses: docker/login-action@v2.0.0
        with:
          registry: ghcr.io
          username: ${{ github.repository_owner }}
          password: ${{ secrets.GITHUB_TOKEN }}

      - name: Build Container Image
        id: docker_build
        uses: docker/build-push-action@v3.0.0
        with:
          context: ./wiki/
          file: ./wiki/Dockerfile
          platforms: linux/amd64
          load: true
          push: false
          tags: ${{ steps.docker_meta.outputs.tags }}
          labels: ${{ steps.docker_meta.outputs.labels }}
          cache-to: type=gha,mode=max
          
      - uses: azure/container-scan@v0.1
        id: container-image-scan
        continue-on-error: true
        with:
          image-name: ${{ steps.docker_meta.outputs.tags }}

      # https://github.com/Azure/container-scan/issues/95#issuecomment-962506492
      - name: Convert Container Scan Report to SARIF
        id: scan-to-sarif
        uses: rm3l/container-scan-to-sarif-action@v1.7.0
        if: ${{ always() }}
        with:
          input-file: ${{ steps.container-image-scan.outputs.scan-report-path }}

      - name: Upload SARIF reports to GitHub Security tab
        uses: github/codeql-action/upload-sarif@v2
        if: ${{ always() }}
        with:
          sarif_file: ${{ steps.scan-to-sarif.outputs.sarif-report-path }}
          
      - name: Push Container Image
        id: docker_push
        uses: docker/build-push-action@v3.0.0
        with:
          context: ./wiki/
          file: ./wiki/Dockerfile
          platforms: linux/amd64
          push: ${{ github.event_name != 'pull_request' }}
          tags: ${{ steps.docker_meta.outputs.tags }}
          labels: ${{ steps.docker_meta.outputs.labels }}
          cache-from: type=gha
